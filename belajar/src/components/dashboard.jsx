import React, { Component } from "react";
import Card from "./common/card/card";
import "./styles.css";
import Sidebar from "./common/sidebar/sidebar";
import Navbar from "./common/navbar/navbar";
import Table from "./common/table/table"

class Dashboard extends Component {
  state = {
    status: {
      sidebar: "block",
      content: "col-10"
    },
    bodys :[
      {
        body1:"Awes",
        body2:"AB11",
        body3:"2210101010"
      },{
        body1:"wesley",
        body2:"AA11",
        body3:"2210202020"
      },{
        body1:"asdf",
        body2:"AA22",
        body3:"2210303030"
      } 
    ],
    headers :[
      {
        id:1,
        headerName: "Nama"
      },
      {
        id:2,
        headerName : "Kelas",
      },
      {
        id:3,
        headerName : "NIM",
      },{
        id:4,
        headerName: "BUTTON"
      }
    ],
    sidebars: [
      {
        id: 1,
        name: "Menu 1",
        icon: "chevron-circle-down"
      },
      {
        id: 2,
        name: "Menu 2",
        icon: "chevron-circle-down"
      }
    ],
    cards: [
      {
        id: 1,
        name: "Card 1",
        total: "150",
        icon: "shopping-bag",
        color: "success"
      },
      {
        id: 2,
        name: "Card 2",
        total: "90",
        icon: "users",
        color: "primary"
      },
      {
        id: 3,
        name: "Card 3",
        total: "45",
        icon: "bitcoin",
        color: "danger"
      },
      {
        id: 4,
        name: "Card 3",
        total: "45",
        icon: "male",
        color: "warning"
      }
    ]
  };

  constructor() {
    super();
    this.toggleSidebar = this.toggleSidebar.bind(this);
  }

  toggleSidebar() {
    const status = {
      sidebar: this.state.status.sidebar === "block" ? "none" : "block",
      content: this.state.status.content === "col-10" ? "col-12" : "col-10"
    };

    this.setState({ status: status });
  }

  render() {
    return (
      <div id="wrapper" className="row">
        <Sidebar
          status={this.state.status.sidebar}
          sidebars={this.state.sidebars}
        />
        <div className={this.state.status.content}>
          <div className="container-fluid">
            <Navbar onToggle={this.toggleSidebar} />
            <div className="card-deck">
              {this.state.cards.map(card => (
                <Card key={card.id} card={card} />
              ))}
            </div>
            <div>
              <Table
              headers={this.state.headers} id={this.state.id} bodys={this.state.bodys} 
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
