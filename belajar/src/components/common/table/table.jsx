import React, { Component } from 'react';
import Header from "./header"
import Body from "./body"

class Table extends Component {
    render() { 
        return ( 
            <div >
                <table>
                 {this.props.headers.map(header => (
                    <th style={{ border:"1px solid black" }}>
                        <Header header={header} />
                    </th>
                    
                ))}
                {this.props.bodys.map(body =>(  
                        <Body body={body}/>  
                ))}
                </table>
                
            </div> 
            );
    }
}
 
export default Table ;