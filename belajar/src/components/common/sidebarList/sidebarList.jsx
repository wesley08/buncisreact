import React from "react";
import "./sidebarList.css";

const SidebarList = props => {
  return (
    <div className="row sidebar-list">
      <div className="col-3">
        <h4>
          <i className={"fa fa-" + props.sidebar.icon} />
        </h4>
      </div>
      <div className="col-9">
        <h4>{props.sidebar.name}</h4>
      </div>
    </div>
  );
};

export default SidebarList;
